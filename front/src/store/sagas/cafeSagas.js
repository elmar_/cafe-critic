import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import loadingSlice from "../slices/loadingSlice";
import cafeSlice from "../slices/cafeSlice";
import {historyPush} from "../actions/historyActions";
import {notificationError} from "../../notification";

const {
  getCafesError,
  getCafesRequest,
  getCafesSuccess,
  postCafeSuccess,
  postCafeError,
  postCafeRequest,
  getOneError,
  getOneRequest,
  getOneSuccess,
  postImageRequest,
  postImageFailure,
  postImageSuccess,
  postReviewFailure,
  postReviewRequest,
  postReviewSuccess,
  deleteCafeRequest,
  deleteCafeSuccess,
  deleteImageRequest,
  deleteImageSuccess,
  deleteReviewRequest,
  deleteReviewSuccess,
} = cafeSlice.actions;

const {loadingRequest} = loadingSlice.actions;


export function* getCafes() {
  try {
    yield put(loadingRequest(true));
    const response = yield axiosApi.get('/cafe');
    yield put(getCafesSuccess(response.data));
    yield put(loadingRequest(false));
  } catch (error) {
    yield put(loadingRequest(false));
    yield put(getCafesError(error.response.data));
  }
}

export function* postCafe({payload: cafe}) {
  const formData = new FormData();
  Object.keys(cafe).forEach(key => {
    formData.append(key, cafe[key]);
  });
  try {
    yield put(loadingRequest(true));
    yield axiosApi.post('/cafe', formData);
    yield put(postCafeSuccess());
    yield put(loadingRequest(false));
    yield put(historyPush('/'));
  } catch (error) {
    yield put(loadingRequest(false));
    yield put(postCafeError(error.response.data));
  }
}

export function* getOneCafe({payload: id}) {
  try {
    yield put(loadingRequest(true));
    const res = yield axiosApi.get('/cafe/' + id);
    yield put(getOneSuccess(res.data));
    yield put(loadingRequest(false));
  } catch (error) {
    yield put(loadingRequest(false));
    yield put(getOneError(error.response.data));
  }
}

export function* postReview({payload: data}) {
  try {
    yield put(loadingRequest(true));
    const res = yield axiosApi.post('/cafe/review', data);
    yield put(postReviewSuccess(res.data));
    yield put(loadingRequest(false));
  } catch (error) {
    yield put(loadingRequest(false));
    yield put(postReviewFailure(error.response.data));
  }
}

export function* postImage({payload: data}) {
  const formData = new FormData();
  Object.keys(data).forEach(key => {
    formData.append(key, data[key]);
  });
  try {
    yield put(loadingRequest(true));
    const res = yield axiosApi.post('/cafe/image', formData);
    yield put(postImageSuccess(res.data));
    yield put(loadingRequest(false));
  } catch (error) {
    yield put(loadingRequest(false));
    yield put(postImageFailure(error.response.data));
  }
}

export function* deleteCafe({payload: id}) {
  try {
    yield put(loadingRequest(true));
    const res = yield axiosApi.delete('/cafe/' + id);
    yield put(deleteCafeSuccess(res.data));
    yield put(loadingRequest(false));
  } catch (error) {
    yield put(loadingRequest(false));
    notificationError('error in delete cafe');
  }
}

export function* deleteReview({payload: data}) {
  try {
    yield put(loadingRequest(true));
    const res = yield axiosApi.post('/cafe/delete/review', data);
    yield put(deleteReviewSuccess(res.data));
    yield put(loadingRequest(false));
  } catch (error) {
    yield put(loadingRequest(false));
    notificationError('error in delete review');
  }
}

export function* deleteImage({payload: data}) {
  try {
    yield put(loadingRequest(true));
    const res = yield axiosApi.post('/cafe/delete/image', data);
    yield put(deleteImageSuccess(res.data));
    yield put(loadingRequest(false));
  } catch (error) {
    yield put(loadingRequest(false));
    notificationError('error in delete image');
  }
}



const cafeSagas = [
  takeEvery(getCafesRequest, getCafes),
  takeEvery(postCafeRequest, postCafe),
  takeEvery(getOneRequest, getOneCafe),
  takeEvery(postReviewRequest, postReview),
  takeEvery(postImageRequest, postImage),
  takeEvery(deleteCafeRequest, deleteCafe),
  takeEvery(deleteReviewRequest, deleteReview),
  takeEvery(deleteImageRequest, deleteImage),
];

export default cafeSagas;
