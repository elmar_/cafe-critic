import {put, takeEvery} from "redux-saga/effects";
import usersSlice from "../slices/usersSlice";
import {historyPush} from "../actions/historyActions";
import axiosApi from "../../axiosApi";
import {notificationError, notificationSuccess} from "../../notification";
import loadingSlice from "../slices/loadingSlice";

const {
  loginFailure,
  loginRequest,
  loginSuccess, logoutRequest, logoutSuccess,
  registerFailure,
  registerRequest,
  registerSuccess,
} = usersSlice.actions;

const {loadingRequest} = loadingSlice.actions;


export function* registerUser({payload: userData}) {
  try {
    yield put(loadingRequest(true));
    const response = yield axiosApi.post('/users', userData);
    yield put(registerSuccess(response.data));
    yield put(loadingRequest(false));
    yield put(historyPush('/'));
  } catch (error) {
    yield put(loadingRequest(false));
    yield put(registerFailure(error.response.data));
  }
}

export function* loginUser({payload: userData}) {
  try {
    yield put(loadingRequest(true));
    const response = yield axiosApi.post('/users/sessions', userData);
    yield put(loginSuccess(response.data.user));
    yield put(loadingRequest(false));
    yield put(historyPush('/'));
    notificationSuccess('Login successful');
  } catch (error) {
    yield put(loadingRequest(false));
    yield put(loginFailure(error.response.data));
  }
}

export function* logout() {
  try {
    yield put(loadingRequest(true));
    yield axiosApi.delete('/users/sessions');
    yield put(logoutSuccess());
    yield put(loadingRequest(false));
    yield put(historyPush('/'));
  } catch (e) {
    yield put(loadingRequest(false));
    notificationError('Logout failed');
  }
}


const usersSagas = [
  takeEvery(registerRequest, registerUser),
  takeEvery(loginRequest, loginUser),
  takeEvery(logoutRequest, logout),
];

export default usersSagas;
