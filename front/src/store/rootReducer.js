import {combineReducers} from "redux";
import usersSlice from "./slices/usersSlice";
import loadingSlice from "./slices/loadingSlice";
import cafeSlice from "./slices/cafeSlice";

const rootReducer = combineReducers({
  users: usersSlice.reducer,
  loading: loadingSlice.reducer,
  cafe: cafeSlice.reducer,
});

export default rootReducer;
