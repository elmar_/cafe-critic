import {all} from "redux-saga/effects";
import historySagas from "./sagas/historySagas";
import history from "../history";
import usersSagas from "./sagas/usersSagas";
import cafeSagas from "./sagas/cafeSagas";


function* rootSaga() {
  yield all([
    ...historySagas(history),
    ...usersSagas,
    ...cafeSagas
  ]);
}

export default rootSaga;
