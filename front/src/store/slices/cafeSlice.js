import {createSlice} from "@reduxjs/toolkit";
import {notificationError} from "../../notification";

export const initialState = {
  getCafesError: null,
  getOneCafeError: null,
  postCafeError: null,
  cafes: [],
  cafe: {
    images: [],
    reviews: []
  },
  postReviewError: null,
  postImageError: null
};

const name = 'cafe';

const cafeSlice = createSlice({
  name,
  initialState,
  reducers: {
    getCafesRequest: state => {},
    getCafesSuccess: (state, {payload: cafes}) => {
      state.cafes = cafes;
      state.getCafesError = null;
    },
    getCafesError: (state, {payload: error}) => {
      notificationError(error.message || error.global || "Error in fetching cafes");
      state.getCafesError = error;
    },

    postCafeRequest: state => {},
    postCafeSuccess: state => {
      state.postCafeError = null;
    },
    postCafeError: (state, {payload: error}) => {
      notificationError(error.message || error.global || "Error in posting cafes");
      state.postCafeError = error;
    },

    getOneRequest: () => {},
    getOneSuccess: (state, {payload: cafe}) => {
      state.getOneCafeError = null;
      state.cafe = cafe;
    },
    getOneError: (state, {payload: error}) => {
      notificationError(error.message || error.global || "Error in getting cafe");
      state.getOneCafeError = error;
      state.cafe = null;
    },

    postReviewRequest: () => {},
    postReviewSuccess: (state, {payload: cafe}) => {
      state.cafe = cafe;
      state.postReviewError = null;
    },
    postReviewFailure: (state, {payload: error}) => {
      state.postReviewError = error;
      notificationError(error.message || error.global || "Error in posting review");
    },

    postImageRequest: () => {},
    postImageSuccess: (state, {payload: cafe}) => {
      state.cafe = cafe;
      state.postImageError = null;
    },
    postImageFailure: (state, {payload: error}) => {
      state.postImageError = error;
      notificationError(error.message || error.global || "Error in posting review");
    },

    deleteCafeRequest: () => {},
    deleteCafeSuccess: (state, {payload: cafes}) => {
      state.cafes = cafes;
    },

    deleteReviewRequest: () => {},
    deleteReviewSuccess: (state, {payload: cafe}) => {
      state.cafe = cafe;
    },

    deleteImageRequest: () => {},
    deleteImageSuccess: (state, {payload: cafe}) => {
      state.cafe = cafe;
    },
  }
});

export default cafeSlice;
