import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  registerError: null,
  loginError: null,
  user: null,
};

const name = 'users';

const usersSlice = createSlice({
  name,
  initialState,
  reducers: {
    registerRequest: state => {},
    registerSuccess: (state, {payload: user}) => {
      state.user = user;
      state.registerError = null;
      state.loginError = null;
    },
    registerFailure: (state, {payload: error}) => {
      state.registerError = error;
    },
    loginRequest: state => {},
    loginSuccess: (state, {payload: user}) => {
      state.user = user;
      state.registerError = null;
      state.loginError = null;
    },
    loginFailure: (state, {payload: error}) => {
      state.loginError = error;
    },
    logoutRequest: () => {},
    logoutSuccess: state => {
      state.user = null;
    },
  }
});

export default usersSlice;
