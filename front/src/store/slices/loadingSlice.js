import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  loading: false
};

const name = 'loading';

const loadingSlice = createSlice({
  name,
  initialState,
  reducers: {
    loadingRequest: (state, {payload}) => {
      state.loading = payload;
    },
  }
});

export default loadingSlice;
