import React from 'react';
import {Button, Checkbox, Form, Input, PageHeader, Upload} from "antd";
import {useHistory} from "react-router-dom";
import cafeSlice from "../../store/slices/cafeSlice";
import {useDispatch, useSelector} from "react-redux";

const { TextArea } = Input;

const {postCafeRequest} = cafeSlice.actions;

const formItemLayout = {
  labelCol: {
    xs: { span: 6 },
    sm: { span: 5 },
  },
  wrapperCol: {
    xs: { span: 14 },
    sm: { span: 12 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 3,
      offset: 0,
    },
    sm: {
      span: 6,
      offset: 5,
    },
  },
};

const normFile = e => {
  if (Array.isArray(e)) {
    return e;
  }
  return e && e.fileList;
};

const AddCafe = () => {
  const history = useHistory();
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);

  const onFinish = data => {
    const newData = {
      ...data,
      image: data.image[0].originFileObj,
      id: user._id
    }
    dispatch(postCafeRequest(newData));
  };

  return (
    <div>
      <PageHeader
        title="Add new cafe"
        onBack={() => history.goBack()}
      />
      <Form
        {...formItemLayout}
        form={form}
        onFinish={onFinish}
      >
        <Form.Item
          name="title"
          label="Title"
          rules={[{ required: true, message: 'Please input your title!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="description"
          label="Description"
          rules={[{ required: true, message: 'Please input your description!' }]}
        >
          <TextArea rows={4} />
        </Form.Item>

        <Form.Item
          name="image"
          label="Файл"
          valuePropName="fileList"
          getValueFromEvent={normFile}
          rules={[
            {
              required: true,
              message: 'Please input image!'
            },
          ]}
        >
          <Upload listType="fileList"  beforeUpload={() => false} accept=".jpg,.png,.gif,.jpeg" maxCount={1}>
            <Button>Click to upload</Button>
          </Upload>
        </Form.Item>

        <Form.Item
          name="agreement"
          valuePropName="checked"
          rules={[
            {
              validator: (_, value) =>
                value ? Promise.resolve() : Promise.reject(new Error('Please accept agreement')),
            },
          ]}
          {...tailFormItemLayout}
        >
          <Checkbox>
            I have read the agreement
          </Checkbox>
        </Form.Item>

        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            Post
          </Button>
        </Form.Item>
      </Form>

    </div>
  );
};

export default AddCafe;
