import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import cafeSlice from "../../store/slices/cafeSlice";
import {Button, Col, Form, Image, Input, Rate, Row, Select, Upload} from "antd";
import {apiURL} from "../../config";
import moment from "moment";

const {getOneRequest, postReviewRequest, postImageRequest, deleteReviewRequest, deleteImageRequest} = cafeSlice.actions;

const { TextArea } = Input;
const { Option } = Select;

const normFile = e => {
  if (Array.isArray(e)) {
    return e;
  }
  return e && e.fileList;
};


const Rating = ({overall = 0, quality = 0, service = 0, interior = 0}) => (
  <>
    <div>
      Overall: <Rate value={overall} disabled /> <span>{overall}</span>
    </div>
    <div>
      Quality of food: <Rate value={quality} disabled /> <span>{quality}</span>
    </div>
    <div>
      Service quality: <Rate value={service} disabled /> <span>{service}</span>
    </div>
    <div>
      Interior: <Rate value={interior} disabled /> <span>{interior}</span>
    </div>
  </>
);



const Cafe = props => {
  const dispatch = useDispatch();
  const cafe = useSelector(state => state.cafe.cafe);
  const user = useSelector(state => state.users.user);

  useEffect(() => {
    if (!!props.match.params.id) {
      dispatch(getOneRequest(props.match.params.id));
    }
  }, [props.match.params.id, dispatch])

  const Comment = ({date, username, description, rate, id}) => (
    <div style={{margin: 5, border: "1px solid #ccc", padding: 10}}>
      <div>On {moment(date).format('DD-MM-YY')}, {username} said:</div>
      <p style={{margin: 10}}>{description}</p>
      <Rating overall={rate?.overall} interior={rate?.interior} quality={rate?.quality} service={rate?.service} />
      {user?.role === 'admin' ?
        <Button danger onClick={() => dispatch(deleteReviewRequest({id: id, cafe: cafe._id}))}>Delete</Button>
        : null}
    </div>
  );

  const ReviewContent = () => {
    return cafe?.reviews.map(item => (
      <Comment
        key={item.date}
        rate={item.rate}
        date={item.date}
        description={item.comment}
        username={item?.rate?.user.username}
        id={item._id}
      />
    ))
  };

  const PhotoContent = () => {
    return cafe?.images?.map(item => {
      if (user?.role === 'admin') {
        return <div style={{display: "inline-block", border: '1px solid #000'}} key={item.image}>
          <Image
            src={apiURL + "/" + item?.image}
            alt="img"
            width={200}
          />
          <div>
            <Button
              danger
              onClick={() => dispatch(deleteImageRequest({id: item._id, cafe: cafe._id}))}
            >
              Delete
            </Button>
          </div>
        </div>
      } else {
        return <div style={{border: '1px solid #000', marginRight: 10, display: "inline-block"}} key={item.image}>
          <Image
            src={apiURL + "/" + item?.image}
            alt="img"
            width={200}
          />
        </div>
      }
    });
  };

  const postComment = data => {
    const newData = {
      ...data,
      user: user._id,
      cafe: cafe._id
    };

    dispatch(postReviewRequest(newData));
  };

  const postImage = data => {
    const newData = {
      ...data,
      image: data.image[0].originFileObj,
      user: user._id,
      cafe: cafe._id
    }
    dispatch(postImageRequest(newData));
  };


  return (
    <div style={{paddingTop: 25}}>
      <Row justify="space-between" align="middle">
        <Col span={9}>
          <h1>{cafe?.title}</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum, enim quasi. A ad consectetur consequuntur, et maxime minima molestias obcaecati?
          </p>
        </Col>

        <Col span={11}>
          <img src={apiURL + "/" + cafe?.mainImage} alt={cafe?.title} width="100%" height="auto" />
        </Col>
      </Row>

      <div>
        <h2>Gallery</h2>
        <div>
          <Image.PreviewGroup style={{display: "flex", flexWrap: "wrap"}}>
            {cafe?.images?.length > 0 ? <PhotoContent /> : <p>No photos</p>}
          </Image.PreviewGroup>
        </div>
      </div>

      <div style={{height: 1, backgroundColor: "#000", marginTop: 10}} />

      <div style={{backgroundColor: "#fff", padding: 10}}>
        <h2>Ratings</h2>
        <Rating
          overall={cafe?.rate?.overall}
          interior={cafe?.rate?.interior}
          quality={cafe?.rate?.quality}
          service={cafe?.rate?.service}
        />
      </div>

      <div style={{height: 1, backgroundColor: "#000", marginTop: 10}} />

      <div style={{backgroundColor: "#fff", padding: "5px 10px"}}>
        <h2>Reviews</h2>
        {cafe?.reviews?.length > 0 ? <ReviewContent /> : <p>No reviews</p>}
      </div>

      {!!user && (
        <div>
          <div>
            <h3>Add review</h3>
            <Form
              onFinish={postComment}
            >
              <Form.Item
                name="comment"
                rules={[
                  {
                    required: true,
                    message: 'Please input review!'
                  },
                ]}
              >
                <TextArea rows={4} />
              </Form.Item>

              <Row justify="space-between">
                <Col>
                  <Form.Item
                    name="quality"
                    label="Quality of food"
                    rules={[
                      {
                        required: true,
                        message: 'Please input Service quality of food!'
                      },
                    ]}
                  >
                    <Select style={{width: 80}}>
                      <Option value="5">5</Option>
                      <Option value="4">4</Option>
                      <Option value="3">3</Option>
                      <Option value="2">2</Option>
                      <Option value="1">1</Option>
                    </Select>
                  </Form.Item>
                </Col>

                <Col>
                  <Form.Item
                    name="service"
                    label="Service quality"
                    rules={[
                      {
                        required: true,
                        message: 'Please input service quality!'
                      },
                    ]}
                  >
                    <Select style={{width: 80}}>
                      <Option value="5">5</Option>
                      <Option value="4">4</Option>
                      <Option value="3">3</Option>
                      <Option value="2">2</Option>
                      <Option value="1">1</Option>
                    </Select>
                  </Form.Item>
                </Col>

                <Col>
                  <Form.Item
                    name="interior"
                    label="Interior"
                    rules={[
                      {
                        required: true,
                        message: 'Please input interior!'
                      },
                    ]}
                  >
                    <Select style={{width: 80}}>
                      <Option value="5">5</Option>
                      <Option value="4">4</Option>
                      <Option value="3">3</Option>
                      <Option value="2">2</Option>
                      <Option value="1">1</Option>
                    </Select>
                  </Form.Item>
                </Col>

                <Col>
                  <Button type="primary" htmlType="submit">
                    Submit review
                  </Button>
                </Col>
              </Row>
            </Form>
          </div>

          <div style={{height: 1, backgroundColor: "#000", marginTop: 10}} />

          <div style={{margin: "20px 0 30px"}}>
            <h3>Upload new photo</h3>
            <Form
              onFinish={postImage}
            >
              <Form.Item
                name="image"
                label="Файл"
                valuePropName="fileList"
                getValueFromEvent={normFile}
                rules={[
                  {
                    required: true,
                    message: 'Please input image!'
                  },
                ]}
              >
                <Upload listType="fileList"  beforeUpload={() => false} accept=".jpg,.png,.gif,.jpeg" maxCount={1}>
                  <Button>Click to upload</Button>
                </Upload>
              </Form.Item>

              <Form.Item>
                <Button type="primary" htmlType="submit">
                  Upload
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      )}
    </div>
  );
};

export default Cafe;
