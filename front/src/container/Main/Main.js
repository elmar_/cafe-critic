import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import cafeSlice from "../../store/slices/cafeSlice";
import Card from "../../components/Card/Card";
import {PageHeader} from "antd";

const {getCafesRequest, deleteCafeRequest} = cafeSlice.actions;

const Main = () => {
  const dispatch = useDispatch();
  const cafes = useSelector(state => state.cafe.cafes);
  const user = useSelector(state => state.users.user);

  const deleteCafe = id => {
    dispatch(deleteCafeRequest(id));
  };

  useEffect(() => {
    dispatch(getCafesRequest())
  }, [dispatch]);
  return (
    <div>
      <PageHeader
        title="All cafes"
      />
      <div style={{display: "flex", justifyContent: "space-between"}}>
        {cafes.map(cafe => (
          <Card
            key={cafe._id}
            reviews={cafe.reviews.length}
            photos={cafe.images.length}
            rate={cafe.rate.overall}
            title={cafe.title}
            img={cafe.mainImage}
            id={cafe._id}
            role={user?.role}
            deleteCafe={deleteCafe}
          />
        ))}
      </div>


    </div>
  );
};

export default Main;
