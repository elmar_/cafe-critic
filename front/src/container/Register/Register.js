import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Alert, Button, Col, Form, Input, Row} from "antd";
import usersSlice from "../../store/slices/usersSlice";
import {Link} from "react-router-dom";

const {registerRequest} = usersSlice.actions;

const Register = () => {
  const error = useSelector(state => state.users.registerError);
  const dispatch = useDispatch();

  const onFinish = data => {
    dispatch(registerRequest(data));
  };


  return (
    <div style={{paddingTop: 200}}>
      <Form
        onFinish={onFinish}
      >
        {<Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item>
              {error && <Alert
                message="Error Text"
                description={error.message || error.global || "Error, try again"}
                type="error"
              />}
            </Form.Item>
          </Col>
        </Row>}

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item
              label="Username"
              name="username"
              rules={[
                {
                  required: true,
                  message: 'Please input your username!',
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item
              label="Password"
              name="password"
              rules={[
                {
                  required: true,
                  message: 'Please input your password!',
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9}>
            <Form.Item>
              <Button type="primary" htmlType="submit" style={{marginRight: 20}}>
                Register
              </Button>
            </Form.Item>
          </Col>
        </Row>

        <Row justify="center">
          <Col xs={16} sm={14} md={12} lg={12} xl={9} >
            <Form.Item>
              <Link to='/login'>Do you already have account?</Link>
            </Form.Item>
          </Col>
        </Row>

      </Form>
    </div>
  );
};

export default Register;
