import {notification} from "antd";

export const notificationSuccess = title => {
  notification.success({
    message: 'Success',
    description: title,
  });
};

export const notificationError = title => {
  notification.error({
    message: 'Error',
    description: title,
  });
};
