import React from 'react';
import {Button, Layout as AntLayout, Space} from "antd";
import {Link} from "react-router-dom";
import usersSlice from "../../store/slices/usersSlice";
import {useDispatch, useSelector} from "react-redux";

const {Header} = AntLayout;

const {logoutRequest} = usersSlice.actions;

const Layout = ({children}) => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);

  const logout = () => {
    dispatch(logoutRequest());
  };

  return (
    <AntLayout>
      <Header style={{color: '#fff', display: 'flex', justifyContent: 'space-between', padding: '0 100px'}}>
        <div>
          <Link to='/' style={{textTransform: "uppercase", color: "#fff", fontWeight: 800}}>
            Cafe Critic
          </Link>
        </div>
        {!!user ? <div>
          <Space size={20}>
            <Link to='/addCafe'>Add new cafe</Link>
            <span>Hello {user.username}</span>
            <Button type='primary' onClick={logout}>Logout</Button>
          </Space>
        </div> : <div>
          <Space size={20}>
            <Link to='/register' component={Button} type='primary'>Sign up</Link>
            <Link to='/login' component={Button} type='primary'>Sign in</Link>
          </Space>
        </div>}
      </Header>
      {children}
    </AntLayout>
  );
};

export default Layout;
