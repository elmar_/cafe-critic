import React from 'react';
import {Button, Card as AntCard, Rate} from 'antd';
import noImage from "../../assets/images/no-image.jpg";
import {apiURL} from "../../config";
import {Link} from "react-router-dom";

const Card = ({img, title, reviews, photos, rate, id, role, deleteCafe}) => {
  return (
    <AntCard
      style={{ width: 300, margin: 5}}
      cover={
        <img
          alt="cafe"
          src={(apiURL + "/" + img) || noImage}
        />
      }
    >
      <div><Link to={"cafe/" + id}>{title}</Link></div>
      <Rate value={rate} disabled />
      <div>({reviews} reviews)</div>
      <div>{photos} photos</div>
      {role === 'admin' ? <div>
        <Button danger onClick={() => deleteCafe(id)}>Delete</Button>
      </div> : null}
    </AntCard>
  );
};

export default Card;
