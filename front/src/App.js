import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import {useSelector} from "react-redux";
import {Spin} from "antd";
import {Content} from "antd/es/layout/layout";
import Register from "./container/Register/Register";
import Login from "./container/Login/Login";
import Layout from "./components/Layout/Layout";
import Main from "./container/Main/Main";
import AddCafe from "./container/AddCafe/AddCafe";
import Cafe from "./container/Cafe/Cafe";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props} /> :
    <Redirect to={redirectTo}/>;
};


const App = () => {
  const user = useSelector(state => state.users.user);
  const loading = useSelector(state => state.loading.loading);

  return (
    <Spin tip="Loading..." spinning={loading}>
      <Layout>
        <Content style={{ padding: '0 50px', minHeight: "85vh"}}>
          <Switch>
            <Route path='/' component={Main} exact />
            <Route path='/cafe/:id' component={Cafe} exact />
            <ProtectedRoute
              path="/addCafe"
              exact
              component={AddCafe}
              isAllowed={user}
              redirectTo="/"
            />
            <ProtectedRoute
              path="/register"
              exact
              component={Register}
              isAllowed={!user}
              redirectTo="/"
            />
            <ProtectedRoute
              path="/login"
              exact
              component={Login}
              isAllowed={!user}
              redirectTo="/"
            />
          </Switch>
        </Content>
      </Layout>
    </Spin>
  );
};

export default App;
