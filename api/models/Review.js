const mongoose = require('mongoose');

const ReviewSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  cafe: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Cafe',
    required: true
  },
  rate: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Rate",
    required: true
  },
  date: {
    type: Date,
    required: true,
  },
  comment: {
    type: String,
    required: true
  }
});

const Review = mongoose.model('Review', ReviewSchema);

module.exports = Review;
