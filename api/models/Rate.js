const mongoose = require('mongoose');

const RateSchema = new mongoose.Schema({
  quality: {
    type: Number,
    //required: true
  },
  service: {
    type: Number,
  },
  interior: {
    type: Number,
  },
  overall: {
    type: Number,
  },
  cafe: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Cafe",
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  }
});

const Rate = mongoose.model('Rate', RateSchema);

module.exports = Rate;
