const mongoose = require('mongoose');


const CafeSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: true
  },
  description: {
    type: String,
    required: true,
  },
  mainImage: {
    type: String,
    required: true,
  },
  agreement: {
    type: Boolean,
    required: true,
  },
  rate: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Rate",
    required: true
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  reviews: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Review"
  }],
  images: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Image"
  }],
});

const Cafe = mongoose.model('Cafe', CafeSchema);

module.exports = Cafe;
