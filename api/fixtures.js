const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const {nanoid} = require('nanoid');
const Cafe = require("./models/Cafe");
const Rate = require("./models/Rate");
const Image = require('./models/Image');
const Review = require("./models/Review");

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create({
    username: 'user',
    password: '123',
    token: nanoid(),
    role: 'user',
  }, {
    username: 'admin',
    password: '123',
    token: nanoid(),
    role: 'admin',
  });

  const [rate1, rate2, rate3, rate4] = await Rate.create({
      interior: 5,
      overall: 4,
      quality: 3,
      service: 4,
      user: user
    }, {
      interior: 3,
      overall: 3,
      quality: 3,
      service: 3,
      user: user
    },
    {
      interior: 2,
      overall: 2,
      quality: 2,
      service: 2,
      user: user
    }, {
      interior: 1,
      overall: 1,
      quality: 1,
      service: 1,
      user: user
    })



  const [cafe1, cafe2] = await Cafe.create({
    title: 'Какое-то кафе',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab animi architecto ea et excepturi impedit inventore ipsa iste maiores, minima, modi nihil, nisi officia optio pariatur repellendus saepe. Quae, sunt.',
    agreement: true,
    user: user,
    mainImage: 'fixtures/fixture_1.jpg',
    rate: rate1,
    reviews: [],
    images: []
  },
    {
      title: 'Второй кафе',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab animi architecto ea et excepturi impedit inventore ipsa iste maiores, minima, modi nihil, nisi officia optio pariatur repellendus saepe. Quae, sunt.',
      agreement: true,
      user: user,
      mainImage: 'fixtures/fixture_2.png',
      rate: rate2,
      reviews: [],
      images: []
    },);

  const [image1, image2] = await Image.create({
    user: user,
    cafe: cafe1,
    image: 'fixtures/fixture_2.png'
  }, {
    user: user,
    cafe: cafe2,
    image: 'fixtures/fixture_1.jpg'
  });

  const [review1, review2] = await Review.create({
      cafe: cafe1,
      comment: "To convert a string to an integer parseInt() function is used in javascript. parseInt() function returns Nan( not a number) when the string doesn’t contain number.",
      date: new Date(),
      rate: rate3,
      user: user
    },
    {
      cafe: cafe2,
      comment: "To convert a string to an integer parseInt() function is used in javascript. parseInt() function returns Nan( not a number) when the string doesn’t contain number.",
      date: new Date(),
      rate: rate4,
      user: user
    });
  const newCafe1 = await Cafe.findOne({_id: cafe1});
  const newCafe2 = await Cafe.findOne({_id: cafe2});

  newCafe1.images.push(image1);
  newCafe1.reviews.push(review1);
  newCafe2.images.push(image2);
  newCafe2.reviews.push(review2);

  await newCafe1.save();
  await newCafe2.save();

  await mongoose.connection.close();
};

run().catch(e => console.error(e));
