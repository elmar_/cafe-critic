const express = require('express');
const auth = require("../middleware/auth");
const upload = require('../multer').cafes;
const Cafe = require('../models/Cafe');
const permit = require("../middleware/permit");
const Rate = require("../models/Rate");
const Review = require("../models/Review");
const Image = require("../models/Image");

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const cafes = await Cafe.find().populate('rate');
    return res.send(cafes);
  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in get cafes'});
  }
});

router.get('/:id', async (req, res) => {
  try {
    const cafe = await Cafe.findOne({_id: req.params.id})
      .populate([{path: 'reviews', populate: {path: "rate"}}
        , {path: 'rate', populate: {path: 'user', select: "username"}}]).populate('images');

    return res.send(cafe);
  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in getOne cafe'});
  }
});

router.post('/review', auth, upload.single('image'), async (req, res) => {
  try {
    const data = req.body;

    const cafe = await Cafe.findOne({_id: data.cafe})
      .populate({path: 'reviews', select: ['comment', 'date', 'rate'],
        populate: {path: "rate", populate: {path: 'user', select: "username"}}}).populate('images');

    const overall = (parseInt(data.quality) + parseInt(data.service) + parseInt(data.interior)) / 3;

    const rate = new Rate({
      quality: data.quality,
      service: data.service,
      interior: data.interior,
      overall: overall,
      cafe: data.cafe,
      user: data.user,
    });
    await rate.save();

    const reviewData = {
      user: data.user,
      cafe: data.cafe,
      rate: rate,
      date: new Date(),
      comment: data.comment
    };

    const review = new Review(reviewData);
    await review.save();

    const cafeRate = {
      quality: 0,
      service: 0,
      interior: 0,
      overall: 0,
      user: data.cafe
    };

    const rates = await Rate.find({cafe: data.cafe});

    rates.forEach(item => {
      cafeRate.quality = cafeRate.quality + item.quality;
      cafeRate.service = cafeRate.service + item.service;
      cafeRate.interior = cafeRate.interior + item.interior;
      cafeRate.overall = (cafeRate.quality + cafeRate.service + cafeRate.interior) / 3;
    });

    const length = rates.length;
    cafeRate.quality = (cafeRate.quality / length).toFixed(1);
    cafeRate.service = (cafeRate.service / length).toFixed(1);
    cafeRate.interior = (cafeRate.interior / length).toFixed(1);
    cafeRate.overall = (cafeRate.overall / length).toFixed(1);



    cafe.reviews.push(review);
    const newRate = new Rate(cafeRate);
    await newRate.save()
    cafe.rate = newRate;
    await cafe.save();
    res.send(cafe);
  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in post review'});
  }
});

router.post('/image', auth, upload.single('image'), async (req, res) => {
  try {
    const data = req.body;
    const cafe = await Cafe.findOne({_id: data.cafe})
      .populate({path: 'reviews', select: ['comment', 'date', 'rate'],
        populate: {path: "rate", populate: {path: 'user', select: "username"}}}).populate('images');

    const createData = {
      cafe: data.cafe,
      user: data.user,
    }

    if (req.file) {
      createData.image = req.file.filename;
    }

    const image = new Image(createData);

    await image.save();

    cafe.images.push(image);
    await cafe.save();

    res.send(cafe);
  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in post cafes'});
  }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const data = req.body;
    if (!data.agreement) {
      return res.status(401).send({message: 'agreement validation'});
    }

    const rate = new Rate({
      quality: 0,
      service: 0,
      interior: 0,
      overall: 0,
      user: data.id,
    });

    const cafeData = {
      title: data.title,
      description: data.description,
      user: data.id,
      agreement: data.agreement,
      rate: rate,
    };

    if (req.file) {
      cafeData.mainImage = req.file.filename;
    }

    const cafe = new Cafe(cafeData);
    await rate.save();
    await cafe.save();



    return res.send(cafe);
  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in post cafes'});
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    await Cafe.findOneAndDelete({_id: req.params.id});
    const cafes = await Cafe.find();
    return res.send(cafes);
  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in delete cafes'});
  }
});

router.post('/delete/review', auth, permit('admin'), async (req, res) => {
  try {
    await Review.findOneAndDelete({_id: req.params.id});
    const cafe = await Cafe.findOne({_id: req.body.cafe})
      .populate({path: 'reviews', select: ['comment', 'date', 'rate'],
        populate: {path: "rate", populate: {path: 'user', select: "username"}}}).populate('images');

    const i = cafe.reviews.findIndex(item => item._id.toString() === req.body.id);
    if (i > -1) {
      cafe.reviews.splice(i, 1);
      await cafe.save();
    }

    return res.send(cafe);
  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in delete cafes'});
  }
});

router.post('/delete/image', auth, permit('admin'), async (req, res) => {
  try {
    await Review.findOneAndDelete({_id: req.body.id});
    const cafe = await Cafe.findOne({_id: req.body.cafe})
      .populate({path: 'reviews', select: ['comment', 'date', 'rate'],
        populate: {path: "rate", populate: {path: 'user', select: "username"}}}).populate('images');

    const i = cafe.images.findIndex(item => item._id.toString() === req.body.id);
    if (i > -1) {
      cafe.images.splice(i, 1);
      await cafe.save();
    }
    return res.send(cafe);
  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in delete cafes'});
  }
});



module.exports = router;
