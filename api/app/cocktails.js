const express = require('express');
const auth = require("../middleware/auth");
const upload = require('../multer').cocktails;
const Cocktail = require('../models/Cocktail');
const permit = require("../middleware/permit");

const router = express.Router();

router.get('/', auth, async (req, res) => {
  try {
    const user = req.query.user;
    if (req.user.role === 'admin') {
      if (user) {
        if (user.toString() !== req.user._id.toString()){
          return res.status(401).send({message: 'another user'});
        }
        const cocktails = await Cocktail.find({user});
        return res.send(cocktails);
      } else {
        const cocktails = await Cocktail.find().limit(20);
        return res.send(cocktails);
      }
    } else {
      if (user) {
        if (user.toString() !== req.user._id.toString()){
          return res.status(401).send({message: 'another user'});
        }
        const cocktails = await Cocktail.find({user});
        return res.send(cocktails);
      } else {
        const cocktails = await Cocktail.find({published: true}).limit(20);
        return res.send(cocktails);
      }
    }

  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in get cocktails'});
  }
});

router.get('/:id', auth, async (req, res) => {
  try {
    const cocktail = await Cocktail.findOne({_id: req.params.id});

    return res.send(cocktail);
  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in getOne cocktails'});
  }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const cocktailData = {
      title: req.body.title,
      recipe: req.body.recipe,
      ingredients: req.body.ingredients !== 'undefined' ? JSON.parse(req.body.ingredients) : [],
      user: req.user._id
    };


    if (req.file) {
      cocktailData.image = req.file.filename;
    }

    const cocktail = new Cocktail(cocktailData);
    await cocktail.save();

    res.send(cocktail);
  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in post cocktails'});
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const cocktail = await Cocktail.findOneAndDelete({_id: req.params.id});

    return res.send(cocktail);
  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in delete cocktails'});
  }
});

router.put('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const cocktail = await Cocktail.findOne({_id: req.params.id});
    if (!cocktail) {
      return res.status(401).send({message: 'dont found'});
    }

    cocktail.published = !cocktail.published;
    cocktail.save();

    return res.send(cocktail);
  } catch (e) {
    console.error(e)
    res.status(500).send({message: 'error in put cocktails'});
  }
});


module.exports = router;
