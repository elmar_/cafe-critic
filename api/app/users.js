const express = require('express');
const User = require('../models/User');
const config = require('../config');
const axios = require("axios");
const auth = require("../middleware/auth");
const {downloadAvatar} = require("../utils");
const {nanoid} = require("nanoid");
const upload = require('../multer').avatar;

const router = express.Router();

router.post('/', async (req, res) => {
  try {
    const user = new User({
      password: req.body.password,
      username: req.body.username,
    });

    user.generateToken();
    await user.save();
    return res.send(user);
  } catch (error) {
    console.log(error);
    return res.status(400).send(error);
  }
});

router.post('/sessions', async (req, res) => {
  const user = await User.findOne({username: req.body.username});
    // .populate([{path: 'friends', select: 'displayName'}, {path: 'shared', select: 'displayName'}]);

  if (!user) {
    return res.status(401).send({message: 'Credentials are wrong'});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(401).send({message: 'Credentials are wrong'});
  }

  user.generateToken();
  await user.save();

  return res.send({message: 'Email and password correct!', user});
});

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Success'};

  if (!token) return res.send(success);

  const user = await User.findOne({token});

  if (!user) return res.send(success);

  user.generateToken();

  await user.save();

  return res.send(success);
});


module.exports = router;
